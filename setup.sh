#!/bin/bash

set -e

SCRIPT=`basename $0`
DIR=$(readlink -f $0 | xargs dirname)

HELP="
Usage:
    $SCRIPT [-n|--node <manager|worker>] [-m|--manager_ip <IP address of manager node>]
"


node_type=""
manager_ip=""

while [[ $# > 0 ]]; do
    key="$1"
    case "$key" in
        -n|--node)
	    shift
	    node_type="$1"
	;;
        -m|--manager_ip)
	    shift
	    manager_ip="$1"
	;;
        -h|--help)
            echo "$HELP"
	    exit 0
        ;;
        *)
            echo "ERROR: Bad option: $key"
	    echo "$HELP"
            exit 1
        ;;
    esac
    shift
done

if [[ "$node_type" != "manager" ]] && [[ "$node_type" != "worker" ]] ; then
    echo "ERROR: Node Type must be either 'manager' or 'worker' ..."
    echo "$HELP"
    exit 1
fi

if [[ "$manager_ip" == "" ]]; then
    echo "ERROR: You must specify a manager node IP address..."
    echo "$HELP"
    exit 1
fi

set -x

sudo apt-get -y update
sudo apt-get -y upgrade
sudo apt-get -y install vim


GO_TAR="go1.10.2.linux-armv6l.tar.gz"
wget -P $DIR https://dl.google.com/go/$GO_TAR
sudo rm -rf /usr/local/go || true
sudo tar -C /usr/local -xzf $DIR/$GO_TAR
rm -rf $DIR/$GO_TAR

cd $HOME 
rm -rf go || true 
mkdir go
mkdir go/bin
mkdir go/src
mkdir go/pkg

go version


mkdir -p $GOPATH/src/github.com/skycoin
cd $GOPATH/src/github.com/skycoin
git clone https://github.com/skycoin/skywire.git
cd $GOPATH/src/github.com/skycoin/skywire/cmd
go install ./...

HOME_ESCAPED="${HOME//\//\\\/}"

if [[ "$node_type" == "manager" ]]; then
    sudo systemctl stop skymanager || true
    cp $DIR/skymanager.service $DIR/skymanager.service.tmp
    sed -i "s/{HOME}/$HOME_ESCAPED/g" $DIR/skymanager.service.tmp
    sudo cp $DIR/skymanager.service.tmp /etc/systemd/system/skymanager.service 
    rm $DIR/skymanager.service.tmp
    sudo systemctl daemon-reload
    sudo systemctl enable skymanager
    sudo systemctl start skymanager
fi


sudo systemctl stop skynode || true
cp $DIR/skynode.service $DIR/skynode.service.tmp 
sed -i "s/{MANAGER_IP_ADDRESS}/${manager_ip}/g" $DIR/skynode.service.tmp
sed -i "s/{HOME}/$HOME_ESCAPED/g" $DIR/skynode.service.tmp
sudo cp $DIR/skynode.service.tmp /etc/systemd/system/skynode.service
rm $DIR/skynode.service.tmp
sudo systemctl daemon-reload
sudo systemctl enable skynode
sudo systemctl start skynode
