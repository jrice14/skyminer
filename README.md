# Overview #
Setup a DIY Skyminer using these simple scripts! [Skycoin](Skycoin.com) aims to decentralize the internet, and you can build your very own Skyminer. It currently does the following

* Acts as a node in a VPN network
* Gets rewards for providing VPN capability. 

It might do some more stuff in the future. More details about rewards can be found here: [Testnet Rules](https://github.com/skycoin/skywire/blob/master/testnet_rules.md)


# Hardware #
I set out to build my own. I settled on the following components:


| Item                                                                                                                     | Quantity    | Cost | Comments                                                                         |   |   |
|--------------------------------------------------------------------------------------------------------------------------|-------------|------|----------------------------------------------------------------------------------|---|---|
| [C4Labs Bramble Case - 6 stack](https://www.c4labs.com/product/zebra-bramble-case-raspberry-pi-3-b-color-and-stack-options/)       | 1 | $35  | Great little case with fans. Some difficult assembly.                            |   |   |
| [Raspberry Pi 4](https://www.raspberrypi.org/)                                                                           | 6           | $220 | Newest Raspberry Pi version                                                      |   |   |
| 32GB MicroSD Cards                                                                                                       | 6           | $35  | Could've gotten away with as little as 8GB, but 32GB wasn't much more expensive. |   |   |
| 1ft USB-C cables                                                                                                         | 6           | $20  | Use to power the Pis                                                             |   |   |
| 6 Inch ethernet cables                                                                                                   | 6           | $10  | Plug these bad boys into the switch                                              |   |   |
| [Linksys EA6400 (AC1600) Router](https://www.linksys.com/us/p/P-EA6400/)                                                 | 1           | $32  | You may already have a home router, but another router will keep your network traffic safe and separate. See this [Linksys Article](https://www.linksys.com/ca/support-article?articleNum=132275) Any router should do. This one was on sale at the time.                          |   |   |
| [TP-Link 8-Port Ethernet Switch](https://www.amazon.com/Ethernet-Splitter-Optimization-Unmanaged-TL-SG108/dp/B00A121WN6) | 1           | $16  | I opted for a flat one, so I could stack the case on it.                         |   |   |
| [RavPower 6-Port USB Power Hub](https://www.amazon.com/RAVPower-Charger-Desktop-Charging-Compatible/dp/B00OQ19QYA)       | 1           | $20  | This is capable of powering all 6 Pis                                            |   |   |

Total cost was about $400.

# Preparation #

There are a couple things to do in the beginning to prepare for the real work...

### Jailbreak your router ###

The purpose of this step is to secure your router and add some nice functionality.
This step isn't that important, and you could choose to skip it. I went ahead and tried it. 
I followed this guide to try and install Tomato, an open source router firmware: [Linksys Tomato Guide](https://www.linksysinfo.org/index.php?threads/guide-flash-linksys-ea6300v1-ea6400-ea6500v2-ea6700-ea6900v1-0-1-1-with-tomato.73877/)

Unfortunately, I was never able to get Tomato to install. I had an issue where the firmware would stop uploading the router randomly. So I was left with just [DD-WRT](https://dd-wrt.com/), which was fine...

### Flash SD Cards with Linux Distro ###

You need to pre-intall a Linux distrobution onto your MicroSD cards. 

Here's what I did...

1. Download [Raspbian](https://www.raspberrypi.org/downloads/) -- The official Raspberry Pi OS
2. Download [Balena Etcher](https://www.balena.io/etcher/)
3. Insert MicroSD cards into computer
4. Run Balena Etcher to install the Raspbian image onto the flash drive
5. Enable SSH by creating a file named "ssh" (no extension) on the imaged drive

NOTE: If you do not perform step 5, you will not be able to SSH into your Raspberry Pi.


# Assembly #

I performed the following steps...

| Step | Description                                | Comments                                                                                                                             |   |   |
|------|--------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------|---|---|
| 1    | Put case together                          | I prepped the case before the Pis arrived                                                                                            |   |   |
| 2    | Insert MicroSD Cards                       | I inserted the Raspbian-flashed MicroSD cards into the Pis.                                                                          |   |   |
| 3    | Install Pis into case                      | Attach fan headers to pins 4 and 6 on the GPIO found that the fan headers were too tall for the space provided, so I had to take off some fasteners on the Pi brackets to fit it. |   |   |
| 4    | Connect USB-C Cords to Power               | These supply power, so you'll need em.                                                                                               |   |   |
| 5    | Connect Pis and Switch with ethernet cords | Do it!                                                                                                                               |   |   |
| 6    | Connect Router                             | Connect a LAN port on your home router to the WAN port on your sky-router. Then connect 1 LAN port on the sky-router to the switch   |   |   |

# Networking #

Your network should look like this at this point....

![skyminer_network](resources/skyminer_network.jpg)

I first setup static IP addresses so that I could easily identify each Pi. Here are the steps I took

1. From the computer connected to the sky-router, type the sky-router IP into browser
2. View connected devices. You should see all of your Pis and their MAC-Addresses
3. Add static IP addresses for those Pi MAC-addresses
4. Re-plug the ethernet on each Pi so it gets the new address. 
5. Clear any DHCP leases that still exist for the Pis


If you'd like to give your Pis identifiable hostnames, then...

* Change desired hostname in /etc/hostname
* Replace the old hostname with the new one in /etc/hosts
* Set the hostname in your router's config
* Reboot Pi

# Setting up Skynodes #

Now that each of your Pis is accessible via a static IP, we can SSH into them using [Putty](https://www.chiark.greenend.org.uk/~sgtatham/putty/). 

First, choose one of your Pis to be the manager node. Then SSH in, from the computer connected to the skyrouter, and run this code...
```
git clone https://jrice14@bitbucket.org/jrice14/skyminer.git   
cd skyminer 
bash set_env.sh  
source ~/.bashrc  
bash setup.sh --node manager --manager_ip MANAGER_IP_ADDRESS     # MANAGER_IP_ADDRESS is the IP of this manager node
```
    
Go to MANAGER_IP_ADDRESS:8000 and ensure that the Node Management window is up and running

Now, SSH into the rest of your Pis, and run all the same code as above, except for the setup.sh line (where you will specify worker instead of manager)...
```
bash setup.sh --node worker --manager_ip MANAGER_IP_ADDRESS      # MANAGER_IP_ADDRESS is the IP of this manager node
```
Now all of your nodes should be added to the Node Management window. 

You now have a skymanager service running on your manager node, and a skynode service running on every node (including the manager node. You can manage them like so...

```
sudo systemctl restart [skymanager|skynode]
sudo systemctl stop [skymanager|skynode]
sudo systemctl start [skymanager|skynode]
sudo systemctl status [skymanager|skynode]
```
or...
```
sudo service [skymanager|skynode] restart
sudo service [skymanager|skynode] stop
sudo service [skymanager|skynode] start
sudo service [skymanager|skynode] status
```

If you aren't seeing nodes appear, try using these commands ensure the service is running and functioning properly.

# Port Forwarding #
You can forward some ports in your sky-router's config so that you can manage your Pi's from a home-network computer. Forward the following ports and point them to your MANAGER_IP_ADDRESS...

| Port number | Usage          | Validation                                                               |
|-------------|----------------|--------------------------------------------------------------------------|
| 22          | SSH            | Putty SSH to your MANAGER_IP_ADDRESS, and SSH to other nodes from there. |
| 8000        | Sky-Manager UI | Type MANAGER_IP_ADDRESS:8000 in your browser, and it should show up.     |

Then, on a home-router connected computer, try typing MANAGER_IP_ADDRESS:8000 in a browser, and it should show up. 

Also, you can putty SSH to your MANAGER_IP_ADDRESS, and SSH to other nodes from there.

# Whitelisting #

You've done all the heavy lifting. Now get PAID!

1. Go to https://whitelist.skycoin.com/
2. Register
3. Submit an application and add all of your "public keys" from your node manager window, with some nice pictures
4. Add your skycoin wallet key

# Final Product #

I think it turned out alright...

![skyminer](resources/skyminer.jpg)

# References #

I found some additional references to be very useful...

* [New Discovery Server](https://medium.com/skycoin/skywire-discovery-node-migration-is-complete-f290d78a2d9f)
* [Skywug DIY Pi Miner Guide](https://skywug.net/forum/Thread-Raspberry-Pi-Sky-Miner-Setup-for-noobs-TESTNET-READY)
* [DIY Skyminer Basic Guide](http://diyskyminer.com/raspberry-pi-b-diyskyminer-setup-guide/)
* [Skywug Complete DIY Guide](https://skywug.net/forum/Thread-DIY-Miner-A-complete-guide-for-Hardware-Software-configuration)
* [Official Skyminer networking guide](https://github.com/skycoin/skywire/wiki/Networking-guide-for-the-official-router)

